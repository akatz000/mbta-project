## MBTA North/South Station Departure Board

Test assignment for the MBTA.

### Design

The idea behind the design of this webapp is to consume the MBTA API schedule, trip, and prediction objects, slim it down, and send only the necessary data to the client.  The back end also contains logic to make sure that a minimum number of departures are sent for each station.  Late at night, this will require a second API request made for the next day's data.

I intentionally avoided making requests and doing all of the processing on the front end in order to avoid exposing the API key.  The API ratelimits clients based on the API key being used.  Exposing the key would be asking for a problem.  For example, someone could grab the API key from the browser requests, spam requests with that key to the API, get it ratelimited, and then when there was a legitimate request to be made, it would also be ratelimited.

Since most of the logic in in the backend, the client-side code has very little work to do, and just slides the data right into the proper location, along with a tiny bit of formatting and logic, e.g., if a train status is 'Now Boarding' but no track number is available, leave the status as 'On Time'.

Originally, I wanted to consume a stream, process it on the fly, and output a stream.  While this would have been really cool, it would have been rather hacky in Ruby, and completely unnecessary -- that level of data resolution simply is not required for the commuter rail.

I settled for a compromise: grab data from the API at regular intervals, process it, and stream it out to any connected clients.

### To Do List

This webapp needs styling (badly), tests, error handling, and would probably benefit from a bit more thought into project structure.

### Setup

In order to run everything locally, you will need to get set up with Ruby, Bundler, RubyGems, etc.

You will also need to create a `.env` file in the project root with your MBTA API key, like so:
```
API_KEY=123FAKE_KEY123
```
