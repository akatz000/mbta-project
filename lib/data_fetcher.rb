require 'net/http'

module DataFetcher
  extend self

  API_URL = 'https://api-v3.mbta.com'
  API_ENDPOINT = '/schedules'
  SSTAT_ROUTES = [
    'CR-Fairmount',
    'CR-Worcester',
    'CR-Franklin',
    'CR-Greenbush',
    'CR-Kingston',
    'CR-Middleborough',
    'CR-Needham',
    'CR-Providence',
    'CR-Foxboro'
  ]
  NORTH_ROUTES = [
    'CR-Fitchburg',
    'CR-Haverhill',
    'CR-Lowell',
    'CR-Newburyport'
  ]

  def call(opts)
    select_routes(opts)
    res = make_req(opts)
    if !enough_data?(res, opts['page[limit]'])
      opts.delete('filter[min_time]')
      opts['filter[date]'] = (Date.today + 1).to_s
      add_res = make_req(opts)
      combine_data(res, add_res)
    else
      res
    end
  end

  private
  def make_req(opts)
    uri = build_uri(opts)
    req = build_req(uri)
    send_req(req)
  end

  def build_uri(opts)
    uri = URI("#{API_URL}#{API_ENDPOINT}?#{URI.encode_www_form(opts)}")
  end

  def build_req(uri)
    req = Net::HTTP::Get.new(uri)
    req['X-Api-Key'] = ENV['API_KEY']
    req
  end

  def send_req(req)
    uri = req.uri
    res = Net::HTTP.start(uri.hostname, nil, use_ssl: true) do |http|
      http.request req
    end
  end

  def enough_data?(raw_data, desired_num)
    json_data = JSON.parse(raw_data.body)
    json_data["data"].length >= 10
  end

  def combine_data(res, add_res)
    parsed_res = JSON.parse(res.body)
    parsed_add = JSON.parse(add_res.body)
    if parsed_res.empty?
      add_res
    else
      parsed_res['data'].concat(parsed_add['data'])
      parsed_res['included'].concat(parsed_add['included'])
      res.body = parsed_res.to_json
      res
    end
  end

  def select_routes(opts)
    if opts['filter[stop]'] == 'place-north'
      opts['filter[route]'] = NORTH_ROUTES.join(',')
    else
      opts['filter[route]'] = SSTAT_ROUTES.join(',')
    end
  end
end
