module DataCleaner
  extend self
  def call(raw_data)
    json_data = JSON.parse(raw_data)
    data, included = split_raw_data(json_data) # separate out the requested object and the included objects
    cleaned_data = pick_data(data)
    cleaned_included = pick_included(included)
    # merge cleaned_data and cleaned_included somehow... efficiently
    combine_data(cleaned_data, cleaned_included)
  end

  private
  def pick_data(data)
    cleaned_data = []
    data.each do |data_item|
      cleaned_data << {
        schedule_id: data_item['id'],
        departure_time: data_item.dig('attributes', 'departure_time'),
        trip_id: data_item.dig('relationships', 'trip', 'data', 'id'),
        prediction_id: data_item.dig('relationships', 'prediction', 'data', 'id')
      }
    end
    cleaned_data
  end

  def pick_included(included)
    cleaned_included = {}
    included.each do |item|
      if item['type'] == 'trip'
        cleaned_included[item['id']] = {
          headsign: item.dig('attributes', 'headsign')
          # name: item.dig('attributes', 'name')
        }
      else
        cleaned_included[item['id']] = {
          predicted_departure_time: item.dig('attributes', 'departure_time'),
          status: item.dig('attributes', 'status'),
          track: extract_track_num(item.dig('relationships', 'stop', 'data', 'id'))
        }
      end
    end
    cleaned_included
  end

  def extract_track_num(stop)
    if match = /-(\d{2})/.match(stop)
      match[1]
    end
  end

  def split_raw_data(json_data)
    data = json_data['data']
    included = json_data['included']
    [data, included]
  end

  def combine_data(data, included)
    data.each do |data_item|
      data_item[:headsign] = included.dig(data_item[:trip_id], :headsign)
      data_item[:predicted_departure_time] = included.dig(data_item[:prediction_id], :predicted_departure_time)
      data_item[:status] = included.dig(data_item[:prediction_id], :status)
      data_item[:track] = included.dig(data_item[:prediction_id], :track)
    end
    data
  end
end
