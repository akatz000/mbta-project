module DataPackager
  extend self

  def call
    north_raw = get_data(:north)
    south_raw = get_data(:south)
    cleaned_north = DataCleaner.call(north_raw.body)
    cleaned_south = DataCleaner.call(south_raw.body)
    "event: update\ndata: #{{north: cleaned_north, south: cleaned_south}.to_json}\n\n\n"
  end

  private
  def get_data(station)
    current_time = Time.now
    query_params = {
      'filter[stop]' => station == :north ? 'place-north' : 'place-sstat',
      'filter[direction_id]'=> 0,
      'include'=> 'trip,prediction',
      'page[limit]'=> 10,
      'filter[min_time]'=> TZInfo::Timezone.get('America/New_York').now.strftime('%H:%M'),
      'sort'=> 'departure_time'
    }
    DataFetcher.call(query_params)
  end
end
