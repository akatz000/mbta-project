var evtSource = new EventSource('http://localhost:9292/stream')

evtSource.addEventListener('update', (e) => {
  let jsonData = JSON.parse(e.data);
  DataHandler.call(jsonData);
});

class DataHandler {
  static call(data) {
    const stations = ['north', 'south'];
    stations.forEach((station) => {
      let stationCap = station.charAt(0).toUpperCase() + station.slice(1);
      data[station].forEach((departure, idx) => {
        if (this.findHeadsign(stationCap, idx).length === 0) return;
        this.findHeadsign(stationCap, idx).text(departure.headsign);
        this.findDepartureTime(stationCap, idx).text(this.formattedDepartTime(departure));
        this.findTrackNum(stationCap, idx).text(departure.track || 'TBD');
        this.findStatus(stationCap, idx).text(DataHandler.departStatus(departure) || 'On Time');
      });
    });
  }

  static findHeadsign(stationCap, idx) {
    return $(`div#heading${stationCap}${idx} span.headsign`);
  }

  static findDepartureTime(stationCap, idx) {
    return $(`div#heading${stationCap}${idx} span.departureTime`);
  }

  static findStatus(stationCap, idx) {
    return $(`div#collapse${stationCap}${idx} span.status`);
  }

  static findTrackNum(stationCap, idx) {
    return $(`div#collapse${stationCap}${idx} span.track`);
  }

  static formattedDepartTime(departData) {
    let departTime = departData.predicted_departure_time || departData.departure_time;
    let date = new Date(departTime);
    return date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true});
  }

  static departStatus(departure) {
    if (departure.track) {
      return departure.status;
    } else {
      return 'On Time';
    }
  }
}
