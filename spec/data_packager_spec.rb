require File.expand_path '../spec_helper.rb', __FILE__

# require File.expand_path '../../lib/data_packager.rb', __FILE__
# require File.expand_path '../../lib/data_fetcher.rb', __FILE__

# require 'pry'

RSpec.describe DataPackager do
  before :each do
    raw_res = double('res', body: {foo: 'bar'}.to_json)
    allow(DataFetcher).to receive(:call).with(any_args).and_return raw_res
    allow(DataCleaner).to receive(:call).with(any_args)
  end

  it 'calls DataFetcher twice' do
    expect(DataFetcher).to receive(:call).exactly(2).times

    DataPackager.call
  end

  it 'calls DataCleaner twice' do
    expect(DataCleaner).to receive(:call).exactly(2).times

    DataPackager.call
  end
end
