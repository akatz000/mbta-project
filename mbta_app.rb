require 'sinatra'
require 'sinatra/streaming'

class MbtaApp < Sinatra::Application
  require_relative './lib/data_cleaner'
  require_relative './lib/data_fetcher'
  require_relative './lib/data_packager'

  set :server, :thin

  connections = []

  get '/' do
    erb :main
  end

  get '/stream', provides: 'text/event-stream' do
    stream(:keep_open) do |out|
      connections << out
      connections.reject!(&:closed?)
      out.write(DataPackager.call)
    end
  end

  Thread.new do
    while true
      if connections.length > 0
        data = DataPackager.call
        connections.reject!(&:closed?)
        connections.each do |out|
          out&.write(data)
        end
      end
      sleep 15
    end
  end
end
