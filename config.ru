require 'rubygems'
require 'bundler'

Bundler.require

require './mbta_app.rb'
run MbtaApp
